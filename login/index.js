const { DynamoDBClient, GetItemCommand, UpdateItemCommand } = require("@aws-sdk/client-dynamodb");
const dbClient = new DynamoDBClient({ region: "us-east-1" });
const { v4: uuidv4 } = require('uuid');

let response = {
    headers: {
        "Content-Type": "application/json; charset=utf-8"
    },
    isBase64Encoded: false,
};

const getUser = async (username) => {
    let user = {};
    const params = {
        TableName: "Users",
        Key: {
            "username": { S: username }
        }
    };
    const command = new GetItemCommand(params);
    const resp = await dbClient.send(command);

    return resp.Item;
}

const parseDynamoString = s => s.S;

const createSession = async (user) => {
    const params = {
        TableName: 'Users',
        Key: { username: user.username },
        UpdateExpression: 'SET sessionToken = :r',
        ExpressionAttributeValues: { ':r': { S: uuidv4() } },
        ReturnValues: 'UPDATED_NEW'
    };
    const command = new UpdateItemCommand(params);
    const resp = await dbClient.send(command);

    return parseDynamoString(resp.Attributes.sessionToken);
}

const doLogin = async (username, password) => {
    const user = await getUser(username);

    if (user !== undefined && parseDynamoString(user.password) === password) {
        return await createSession(user);
    } else {
        throw 'Incorrect login credentials';
    }
}

exports.handler = async (request) => {
    const payload = JSON.parse(request.body);

    try {
        const token = await doLogin(payload.username, payload.password);
        response.statusCode = 200;
        response.body = JSON.stringify({
            sessionToken: token
        });
    } catch (err) {
        console.error(err);
        response.statusCode = 404;
        response.body = JSON.stringify({
            error: err
        });
    }

    return response;
};
