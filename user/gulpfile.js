const gulp = require('gulp');
const zip = require('gulp-zip');
const del = require('del');

function clean() {
    return del(['./dist']);
}

function package() {
    return gulp.src(
        [
            './index.js',
            './node_modules/**'
        ],
        {
            base: './'
        }
    )
        .pipe(zip('mazes.zip'))
        .pipe(gulp.dest('./dist'));
}

exports.build = gulp.series(clean, package);
exports.clean = clean;
exports.default = gulp.series(clean, package);
