const { DynamoDBClient, PutItemCommand } = require("@aws-sdk/client-dynamodb");
const dbClient = new DynamoDBClient({ region: "us-east-1" });
const { v4: uuidv4 } = require('uuid');

let response = {
    headers: {
        "Content-Type": "application/json; charset=utf-8"
    },
    isBase64Encoded: false,
};

const addUser = async (username, password) => {
    const params = {
        TableName: "Users",
        Item: {
            id: { S: uuidv4() },
            username: { S: `${username}` },
            password: { S: `${password}` }
        }
    };
    const command = new PutItemCommand(params);

    return await dbClient.send(command);
}

exports.handler = async (request) => {
    const payload = JSON.parse(request.body);

    await addUser(payload.username, payload.password)
        .then(data => {
            if (data["$metadata"].httpStatusCode === 200) {
                response.statusCode = data["$metadata"].httpStatusCode;
                response.body = JSON.stringify({
                    message: "User registered with success."
                });
            }
        })
        .catch(err => {
            response.statusCode = 500,
            response.body = JSON.stringify({
                message: "Something went wrong when registering an user."
            })

            console.log(err)
        });


    return response;
};
